package com.seleniumDemo;

import org.junit.Test;

import java.util.Arrays;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Practica1 {
	
	private WebDriver driver;
	
	@Before
	public void configura() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("");
	}

	@Test
	
	public void test() throws InterruptedException {
		int[][] a = { { 1, 2, -3 }, { 4, 0, -2 } };
	    int[][] b = { { 3, 1 }, { 2, 4 }, { -1, 5 } };
	    int[][] c = multiply(a, b);
	    System.out.println("nudes diego valenzuela" + Arrays.deepToString(c));
	}
	
	public static int[][] multiply(int[][] a, int[][] b) {
	    int[][] c = new int[a.length][b[0].length];

	    if (a[0].length == b.length) {
	        for (int i = 0; i < a.length; i++) {
	            for (int j = 0; j < b[0].length; j++) {
	                for (int k = 0; k < a[0].length; k++) {
	                    c[i][j] += a[i][k] * b[k][j];
	                }
	            }
	        }
	    }
	    return c;
	}
		@After
	public void tearDown(){
	driver.quit();
	}
}
